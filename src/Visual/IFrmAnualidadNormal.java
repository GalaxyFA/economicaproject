package Visual;

import Gestiones.AnuaInt;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class IFrmAnualidadNormal extends javax.swing.JInternalFrame {

    private AnuaInt ai;
    public IFrmAnualidadNormal() {
        initComponents();
        ai= new AnuaInt();
        Diferidos(false);
        Grad(false);
        radPresente.setSelected(true);
        radCreciente.setSelected(true);
        txtResultado.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        Tiempo = new javax.swing.ButtonGroup();
        Gradiente = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbTipo = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        radPresente = new javax.swing.JRadioButton();
        radFuturo = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnCalcular = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtAnualidad = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        txtInteres = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        spinPeriodos = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        txtResultado = new javax.swing.JTextField();
        lbGradiente = new javax.swing.JLabel();
        txtGradiente = new javax.swing.JFormattedTextField();
        lbDel_Tipo = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        radCreciente = new javax.swing.JRadioButton();
        radDecreciente = new javax.swing.JRadioButton();
        lbl_Diferidos = new javax.swing.JLabel();
        spinDiferido = new javax.swing.JSpinner();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Anualidades y Gradientes");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icons/list.png"))); // NOI18N

        java.awt.GridBagLayout jPanel1Layout = new java.awt.GridBagLayout();
        jPanel1Layout.columnWidths = new int[] {0, 16, 0, 16, 0, 16, 0};
        jPanel1Layout.rowHeights = new int[] {0};
        jPanel1.setLayout(jPanel1Layout);

        jLabel1.setText("Seleccione el tipo de anualidad");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jLabel1, gridBagConstraints);

        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ordinaria", "Anticipada", "Diferida", "Perpetua", "Gradiente aritmetrico", "Gradiente Geometrico" }));
        cmbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel1.add(cmbTipo, gridBagConstraints);

        jPanel4.setLayout(new java.awt.BorderLayout());

        Tiempo.add(radPresente);
        radPresente.setText("Presente");
        jPanel4.add(radPresente, java.awt.BorderLayout.CENTER);

        Tiempo.add(radFuturo);
        radFuturo.setText("Futuro");
        jPanel4.add(radFuturo, java.awt.BorderLayout.PAGE_START);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jPanel4, gridBagConstraints);

        jLabel3.setText("Tiempo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jLabel3, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);

        btnCalcular.setText("Calcular");
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        jPanel2.add(btnCalcular);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel2.add(btnCancelar);

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_END);

        java.awt.GridBagLayout jPanel3Layout = new java.awt.GridBagLayout();
        jPanel3Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0};
        jPanel3Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0};
        jPanel3.setLayout(jPanel3Layout);

        jLabel2.setText("Valor de la Anualidad");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        jPanel3.add(jLabel2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel3.add(txtAnualidad, gridBagConstraints);

        jLabel4.setText("tasa de interes");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanel3.add(jLabel4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 8);
        jPanel3.add(txtInteres, gridBagConstraints);

        jLabel5.setText("Numero de periodos");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        jPanel3.add(jLabel5, gridBagConstraints);

        spinPeriodos.setModel(new javax.swing.SpinnerNumberModel(1, 1, 20, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 16;
        jPanel3.add(spinPeriodos, gridBagConstraints);

        jLabel6.setText("Presente o Futuro");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        jPanel3.add(jLabel6, gridBagConstraints);

        txtResultado.setDisabledTextColor(new java.awt.Color(0, 204, 204));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 7);
        jPanel3.add(txtResultado, gridBagConstraints);

        lbGradiente.setText("Gradiente");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel3.add(lbGradiente, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel3.add(txtGradiente, gridBagConstraints);

        lbDel_Tipo.setText("Del tipo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanel3.add(lbDel_Tipo, gridBagConstraints);

        Gradiente.add(radCreciente);
        radCreciente.setText("Creciente");
        jPanel5.add(radCreciente);

        Gradiente.add(radDecreciente);
        radDecreciente.setText("Decreciente");
        jPanel5.add(radDecreciente);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 8);
        jPanel3.add(jPanel5, gridBagConstraints);

        lbl_Diferidos.setText("Periodos diferidos");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 11, 0, 0);
        jPanel3.add(lbl_Diferidos, gridBagConstraints);

        spinDiferido.setModel(new javax.swing.SpinnerNumberModel(1, 1, 15, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipadx = 16;
        jPanel3.add(spinDiferido, gridBagConstraints);

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        setBounds(0, 0, 628, 308);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
         int index = cmbTipo.getSelectedIndex();
        double anua= Double.parseDouble(txtAnualidad.getText());
        double interes= Double.parseDouble(txtInteres.getText());
        int periodo= (Integer) spinPeriodos.getValue();
        double resultado = 0;
        switch(index){
            case 0:{
                //Anualidad ordinaria
                if(radPresente.isSelected()){
                    resultado= ai.PresenteAnuaNormal(anua, interes, periodo);
                }else{
                   resultado= ai.FuturoAnualNormal(anua, interes, periodo);
                }
                break;
            }
            case 1:{
                //Anualidad anticipada
                 if(radPresente.isSelected()){
                    resultado= ai.PresenteAnuaAnticipada(anua, interes, periodo);
                }else{
                   resultado= ai.FuturoAnuaAnticipada(anua, interes, periodo);
                }
                break;
            }
            case 2:{
                //Anualidad Diferida
                int m= (Integer) spinDiferido.getValue();
                if(radPresente.isSelected()){
                 resultado= ai.AnuaDiferida(anua, interes, periodo, m);
                }
                break;
            }
            case 3:{
                //Anualidad Perpetua
                resultado= anua / interes;
                break;
            }
            case 4:{
                //Gradiente arimetica
                double grad= Double.parseDouble(txtGradiente.getText());
                double gra1=0;
                if(radPresente.isSelected()){
                      gra1=ai.PresenteGradienteArimetica(grad, interes, periodo);
                    if(radCreciente.isSelected()){
                     resultado= ai.PresenteAnuaNormal(anua, interes, periodo)+gra1;
                    }else{
                      resultado= ai.PresenteAnuaNormal(anua, interes, periodo)-gra1;   
                    }
                }else{
                    gra1= ai.FuturoGradienteArimetica(grad, interes, periodo);
                    if(radCreciente.isSelected()){
                       resultado= ai.FuturoAnualNormal(anua, interes, periodo)+gra1;
                    }else{
                         resultado= ai.FuturoAnualNormal(anua, interes, periodo)-gra1;
                    }
                }
             break;
            }
            case 5:{
                //Gradiente geometrica
                 double grad= Double.parseDouble(txtGradiente.getText());
                 if(radPresente.isSelected()){
                     resultado=ai.PresenteGradienteGeometrico(anua, interes, grad, periodo);
                 }else{
                     resultado = ai.FuturoGradienteGeometricodouble(anua, interes, grad, periodo);
                 }
              break;  
            }
        }
        txtResultado.setText(Double.toString(resultado));
    }//GEN-LAST:event_btnCalcularActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
      txtAnualidad.setText("");
      txtGradiente.setText("");
      txtInteres.setText("");
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void cmbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoActionPerformed
       int index = cmbTipo.getSelectedIndex();
       switch(index){
            case 2:{
               Diferidos(true);
               Grad(false);
               break;
            }
            case 4:{
                Grad(true);
                Diferidos(false);
               break;
            }
            case 5:{
                Diferidos(false);
                Grad(true);
                radCreciente.setVisible(false);
                radDecreciente.setVisible(false);
                lbDel_Tipo.setVisible(false);
                break;
            }
            default:{
                Diferidos(false);
                Grad(false);
                break;
            }
       }
    }//GEN-LAST:event_cmbTipoActionPerformed

    public void Grad(boolean verd){
        lbDel_Tipo.setVisible(verd);
        lbGradiente.setVisible(verd);
        radCreciente.setVisible(verd);
        radDecreciente.setVisible(verd);
        txtGradiente.setVisible(verd);
    }
    public void Diferidos(boolean verd){
        lbl_Diferidos.setVisible(verd);
        spinDiferido.setVisible(verd);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup Gradiente;
    private javax.swing.ButtonGroup Tiempo;
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lbDel_Tipo;
    private javax.swing.JLabel lbGradiente;
    private javax.swing.JLabel lbl_Diferidos;
    private javax.swing.JRadioButton radCreciente;
    private javax.swing.JRadioButton radDecreciente;
    private javax.swing.JRadioButton radFuturo;
    private javax.swing.JRadioButton radPresente;
    private javax.swing.JSpinner spinDiferido;
    private javax.swing.JSpinner spinPeriodos;
    private javax.swing.JFormattedTextField txtAnualidad;
    private javax.swing.JFormattedTextField txtGradiente;
    private javax.swing.JFormattedTextField txtInteres;
    private javax.swing.JTextField txtResultado;
    // End of variables declaration//GEN-END:variables
}
