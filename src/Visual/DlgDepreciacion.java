package Visual;

import Gestiones.DepreciacionGestion;
import Raiz.Depreciacion;
import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class DlgDepreciacion extends javax.swing.JDialog {
    private DepreciacionGestion dg;
    private Depreciacion dep; 
    
    public DlgDepreciacion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        dg = new DepreciacionGestion();
        initComponents();
        radAnual.setSelected(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        TimeGroup = new javax.swing.ButtonGroup();
        DepTimeGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtActivo = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        txtRescate = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        spinPeriodo = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        radMensual = new javax.swing.JRadioButton();
        radAnual = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Registar datos");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setIconImage(getIconImage());

        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        jPanel1.add(btnAceptar);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancelar);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jLabel1.setFont(new java.awt.Font("Plantagenet Cherokee", 1, 18)); // NOI18N
        jLabel1.setText("Introduzca los Siguientes valores");
        jPanel3.add(jLabel1);

        getContentPane().add(jPanel3, java.awt.BorderLayout.PAGE_START);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("Valor del Activo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 8);
        jPanel4.add(jLabel2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 7);
        jPanel4.add(txtActivo, gridBagConstraints);

        jLabel3.setText("Tiempo de Vida");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 8);
        jPanel4.add(jLabel3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 10);
        jPanel4.add(txtRescate, gridBagConstraints);

        jLabel4.setText("Valor de Rescate");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 7);
        jPanel4.add(jLabel4, gridBagConstraints);

        spinPeriodo.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 8);
        jPanel4.add(spinPeriodo, gridBagConstraints);

        jLabel6.setText("Forma a depreciar");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 10);
        jPanel4.add(jLabel6, gridBagConstraints);

        jPanel5.setLayout(new java.awt.BorderLayout());

        DepTimeGroup.add(radMensual);
        radMensual.setText("Mensual");
        jPanel5.add(radMensual, java.awt.BorderLayout.CENTER);

        DepTimeGroup.add(radAnual);
        radAnual.setText("Anual");
        jPanel5.add(radAnual, java.awt.BorderLayout.PAGE_START);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 10);
        jPanel4.add(jPanel5, gridBagConstraints);

        getContentPane().add(jPanel4, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(409, 239));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
    dg.Actualizar();
    int periodo;
    double activo, rescate,depreciacion, deca = 0;
      
    periodo = (Integer) spinPeriodo.getValue();
    activo = Double.parseDouble(txtActivo.getText());
    rescate = Double.parseDouble(txtRescate.getText());
      
    if(radMensual.isSelected()){
        periodo *= 12;
    }
    
    depreciacion = (activo - rescate) / periodo;
    for(int i=0; i<periodo; i++){
        deca=deca+depreciacion;
        dep= new Depreciacion(i+1, depreciacion, deca, activo- deca);
        dg.GuardarDepreciacion(dep);
    }
    dispose();
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("img/icons/dollar-symbol-1.png"));
        return retValue;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup DepTimeGroup;
    private javax.swing.ButtonGroup TimeGroup;
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JRadioButton radAnual;
    private javax.swing.JRadioButton radMensual;
    private javax.swing.JSpinner spinPeriodo;
    private javax.swing.JFormattedTextField txtActivo;
    private javax.swing.JFormattedTextField txtRescate;
    // End of variables declaration//GEN-END:variables
}
