package Visual;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class IFrmInteres extends javax.swing.JInternalFrame {

    public IFrmInteres() {
        initComponents();
         txtPresente.setEnabled(false);
         Resultado.setEditable(false);
         radSimple.setSelected(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        InteresType = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbMetodo = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        Resultado = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        btnCalcular = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lblPresente = new javax.swing.JLabel();
        txtPresente = new javax.swing.JFormattedTextField();
        lblFuturo = new javax.swing.JLabel();
        txtFuturo = new javax.swing.JFormattedTextField();
        lblInteres = new javax.swing.JLabel();
        txtTde_Interes = new javax.swing.JFormattedTextField();
        lblPeriodo = new javax.swing.JLabel();
        txtPeriodo = new javax.swing.JFormattedTextField();
        jPanel4 = new javax.swing.JPanel();
        radSimple = new javax.swing.JRadioButton();
        radCompuesto = new javax.swing.JRadioButton();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("El dinero en el tiempo");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icons/connection.png"))); // NOI18N

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Formular por:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 40;
        gridBagConstraints.insets = new java.awt.Insets(9, 15, 0, 8);
        jPanel1.add(jLabel1, gridBagConstraints);

        cmbMetodo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Presente", "Futuro", "Tasa de Interes", "Numero de periodos", "Interes", " " }));
        cmbMetodo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbMetodoItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 40;
        gridBagConstraints.insets = new java.awt.Insets(9, 0, 0, 0);
        jPanel1.add(cmbMetodo, gridBagConstraints);

        jLabel2.setText("Resultado");
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.insets = new java.awt.Insets(9, 20, 0, 0);
        jPanel1.add(jLabel2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 30;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(9, 1, 0, 16);
        jPanel1.add(Resultado, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 18, 5));

        btnCalcular.setText("Calcular");
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        jPanel2.add(btnCalcular);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel2.add(btnCancelar);

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_END);

        java.awt.GridBagLayout jPanel3Layout = new java.awt.GridBagLayout();
        jPanel3Layout.columnWidths = new int[] {0, 16, 0, 16, 0, 16, 0, 16, 0, 16, 0};
        jPanel3Layout.rowHeights = new int[] {0, 17, 0, 17, 0};
        jPanel3.setLayout(jPanel3Layout);

        lblPresente.setText("Presente");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel3.add(lblPresente, gridBagConstraints);

        txtPresente.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel3.add(txtPresente, gridBagConstraints);

        lblFuturo.setText("Futuro");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel3.add(lblFuturo, gridBagConstraints);

        txtFuturo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 7);
        jPanel3.add(txtFuturo, gridBagConstraints);

        lblInteres.setText("Tasa de interes");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        jPanel3.add(lblInteres, gridBagConstraints);

        txtTde_Interes.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#.00"))));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel3.add(txtTde_Interes, gridBagConstraints);

        lblPeriodo.setText("Numero de periodo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel3.add(lblPeriodo, gridBagConstraints);

        try {
            txtPeriodo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 7);
        jPanel3.add(txtPeriodo, gridBagConstraints);

        InteresType.add(radSimple);
        radSimple.setText("Simple");
        jPanel4.add(radSimple);

        InteresType.add(radCompuesto);
        radCompuesto.setText("Compuesto");
        jPanel4.add(radCompuesto);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 13, 0, 0);
        jPanel3.add(jPanel4, gridBagConstraints);

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        setBounds(0, 0, 584, 258);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
     txtPresente.setText("");
     txtFuturo.setText("");
     txtPeriodo.setText("");
     txtTde_Interes.setText((""));
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void cmbMetodoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbMetodoItemStateChanged
     int index= cmbMetodo.getSelectedIndex(); 
     switch(index){
         case 0:{
             txtPresente.setEnabled(false);
             txtFuturo.setEnabled(true);
             txtPeriodo.setEnabled(true);
             txtTde_Interes.setEnabled(true);
             break;
         }
         case 1:{
             txtPresente.setEnabled(true);
             txtFuturo.setEnabled(false);
             txtPeriodo.setEnabled(true);
             txtTde_Interes.setEnabled(true);
             break;
         }
         case 2:{
             txtPresente.setEnabled(true);
             txtFuturo.setEnabled(true);
             txtPeriodo.setEnabled(true);
             txtTde_Interes.setEnabled(false); 
             break;
         }
         case 3:{
             txtPresente.setEnabled(true);
             txtFuturo.setEnabled(true);
             txtPeriodo.setEnabled(false);
             txtTde_Interes.setEnabled(true); 
             break;
         }
      }
    }//GEN-LAST:event_cmbMetodoItemStateChanged

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
      int index= cmbMetodo.getSelectedIndex(),periodo = 0;
      double resultado=0,futuro=0,presente=0,interes=0;
          futuro=setVariables(2);
          presente=setVariables(1);
          interes=setVariables(3);
          try{
              periodo= Integer.parseInt(txtPeriodo.getText());
          }catch(NumberFormatException e){}     
        switch(index){
             case 0:{
              //presente
              if(radSimple.isSelected()){
                resultado= futuro*(1/(1+(interes*periodo))) ; 
              }else{
                resultado= futuro* Math.pow((1+interes),- periodo);
              }
              break;
             }
             case 1:{
                //Futuro
               
              if(radSimple.isSelected()){
                  resultado= presente*(1+(interes*periodo));
              }else{
                  resultado= presente * Math.pow((1+interes), periodo);
              }
              break;
             }
             case 2:{
             //tasa de interes 
             if(radSimple.isSelected()){
                 resultado= (Math.exp(Math.log(futuro/presente)/periodo))-1;
             }else{
                 resultado= Math.pow((futuro/presente),1/periodo)-1;
             }
             break;
            }
             case 3:{
              //numero de periodos
              break;
             }
             case 4:{
              //Interes
              if(radSimple.isSelected()){
                  resultado= presente*interes*periodo;
              }else{
                  resultado=presente*(1+(interes*periodo));
              }
              break;
             }
      }
        
       Resultado.setText(Double.toString(resultado));    
    }//GEN-LAST:event_btnCalcularActionPerformed

    public double setVariables(int a){
        double var = 0;
        
        switch(a){
            case 1:{
                try{
                    var=Double.parseDouble(txtPresente.getText());
                 }catch(NumberFormatException ex){  }
                break;
            }
            case 2:{
                try{
                    var=Double.parseDouble(txtFuturo.getText());
                }catch(NumberFormatException ex){ }
                break;
            }
            case 3:{
                try{
                    var=Double.parseDouble(txtTde_Interes.getText());
                }catch(NumberFormatException ex){ }
                break;
            }
        }
        return var;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup InteresType;
    private javax.swing.JTextField Resultado;
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox<String> cmbMetodo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel lblFuturo;
    private javax.swing.JLabel lblInteres;
    private javax.swing.JLabel lblPeriodo;
    private javax.swing.JLabel lblPresente;
    private javax.swing.JRadioButton radCompuesto;
    private javax.swing.JRadioButton radSimple;
    private javax.swing.JFormattedTextField txtFuturo;
    private javax.swing.JFormattedTextField txtPeriodo;
    private javax.swing.JFormattedTextField txtPresente;
    private javax.swing.JFormattedTextField txtTde_Interes;
    // End of variables declaration//GEN-END:variables
}
