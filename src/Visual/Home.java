package Visual;

import Controladores.ControladorPrincipal;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class Home extends javax.swing.JFrame {
    private ControladorPrincipal cp;
    
    public Home() {
        cp = new ControladorPrincipal();
        try {
           javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
           JOptionPane.showMessageDialog(this, ex.toString(), "Error Look and Feels", JOptionPane.ERROR_MESSAGE);
        }
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Desktop = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        Menu_Inte_and_Anual = new javax.swing.JMenu();
        Interes = new javax.swing.JMenuItem();
        AnuaGrad = new javax.swing.JMenuItem();
        Menu_Depreciacion = new javax.swing.JMenu();
        Despre_Ascendente = new javax.swing.JMenuItem();
        Menu_Eva = new javax.swing.JMenu();
        Eva_VPN = new javax.swing.JMenuItem();
        Eva_TIR = new javax.swing.JMenuItem();
        Eva_PR = new javax.swing.JMenuItem();
        Eva_IR = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Proyecto de Prog.");
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setIconImage(getIconImage());

        Desktop.setBackground(new java.awt.Color(204, 204, 204));
        Desktop.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        Desktop.setForeground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout DesktopLayout = new javax.swing.GroupLayout(Desktop);
        Desktop.setLayout(DesktopLayout);
        DesktopLayout.setHorizontalGroup(
            DesktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 411, Short.MAX_VALUE)
        );
        DesktopLayout.setVerticalGroup(
            DesktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 277, Short.MAX_VALUE)
        );

        getContentPane().add(Desktop, java.awt.BorderLayout.CENTER);

        Menu_Inte_and_Anual.setText("Valor y Anua.");

        Interes.setText("Valor del dinero");
        Interes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InteresActionPerformed(evt);
            }
        });
        Menu_Inte_and_Anual.add(Interes);

        AnuaGrad.setText("Anualidad y Gradiente");
        AnuaGrad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnuaGradActionPerformed(evt);
            }
        });
        Menu_Inte_and_Anual.add(AnuaGrad);

        jMenuBar1.add(Menu_Inte_and_Anual);

        Menu_Depreciacion.setText("Depreciación");

        Despre_Ascendente.setText("Depreciación");
        Despre_Ascendente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Despre_AscendenteActionPerformed(evt);
            }
        });
        Menu_Depreciacion.add(Despre_Ascendente);

        jMenuBar1.add(Menu_Depreciacion);

        Menu_Eva.setText("Evaluación");

        Eva_VPN.setText("VPN");
        Eva_VPN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Eva_VPNActionPerformed(evt);
            }
        });
        Menu_Eva.add(Eva_VPN);

        Eva_TIR.setText("TIR");
        Eva_TIR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Eva_TIRActionPerformed(evt);
            }
        });
        Menu_Eva.add(Eva_TIR);

        Eva_PR.setText("PR");
        Menu_Eva.add(Eva_PR);

        Eva_IR.setText("IR");
        Eva_IR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Eva_IRActionPerformed(evt);
            }
        });
        Menu_Eva.add(Eva_IR);

        jMenuBar1.add(Menu_Eva);

        setJMenuBar(jMenuBar1);

        setBounds(0, 0, 433, 354);
    }// </editor-fold>//GEN-END:initComponents

    private void InteresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InteresActionPerformed
       IFrmInteres ifi = new IFrmInteres();
        Desktop.add(ifi);
        ifi.setVisible(true);
    }//GEN-LAST:event_InteresActionPerformed

    private void Despre_AscendenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Despre_AscendenteActionPerformed
        FrmDepreciacion da= new FrmDepreciacion();
        Desktop.add(da);
        da.setVisible(true);
    }//GEN-LAST:event_Despre_AscendenteActionPerformed

    private void Eva_VPNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Eva_VPNActionPerformed
        VPN vpn= new VPN();
        Desktop.add(vpn);
        vpn.setVisible(true);
    }//GEN-LAST:event_Eva_VPNActionPerformed

    private void Eva_TIRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Eva_TIRActionPerformed
      TIR tir = new TIR();
      Desktop.add(tir);
      tir.setVisible(true);
    }//GEN-LAST:event_Eva_TIRActionPerformed

    private void AnuaGradActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnuaGradActionPerformed
         IFrmAnualidadNormal ifan= new IFrmAnualidadNormal();
        Desktop.add(ifan);
        ifan.setVisible(true);
    }//GEN-LAST:event_AnuaGradActionPerformed

    private void Eva_IRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Eva_IRActionPerformed
        IR ir= new IR();
        Desktop.add(ir);
        ir.setVisible(true);
    }//GEN-LAST:event_Eva_IRActionPerformed

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(() -> {
            new Home().setVisible(true);
        });
    }
    
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("img/icons/browser.png"));
        return retValue;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem AnuaGrad;
    private javax.swing.JDesktopPane Desktop;
    private javax.swing.JMenuItem Despre_Ascendente;
    private javax.swing.JMenuItem Eva_IR;
    private javax.swing.JMenuItem Eva_PR;
    private javax.swing.JMenuItem Eva_TIR;
    private javax.swing.JMenuItem Eva_VPN;
    private javax.swing.JMenuItem Interes;
    private javax.swing.JMenu Menu_Depreciacion;
    private javax.swing.JMenu Menu_Eva;
    private javax.swing.JMenu Menu_Inte_and_Anual;
    private javax.swing.JMenuBar jMenuBar1;
    // End of variables declaration//GEN-END:variables
}
