package Visual;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class PR extends javax.swing.JInternalFrame {

    public PR() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtInversion = new javax.swing.JFormattedTextField();
        Pago1 = new javax.swing.JLabel();
        txtPago1 = new javax.swing.JFormattedTextField();
        Pago2 = new javax.swing.JLabel();
        txtPago2 = new javax.swing.JFormattedTextField();
        Pago3 = new javax.swing.JLabel();
        txtPago3 = new javax.swing.JFormattedTextField();
        Pago4 = new javax.swing.JLabel();
        txtPago4 = new javax.swing.JFormattedTextField();
        cmbPagos = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        txtTiempo = new javax.swing.JTextField();

        setClosable(true);
        setMaximizable(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 394, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("Inversión");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weighty = 0.1;
        jPanel2.add(jLabel2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 3;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        jPanel2.add(txtInversion, gridBagConstraints);

        Pago1.setText("Pago 1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel2.add(Pago1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 5);
        jPanel2.add(txtPago1, gridBagConstraints);

        Pago2.setText("Pago 2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel2.add(Pago2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 5);
        jPanel2.add(txtPago2, gridBagConstraints);

        Pago3.setText("Pago 3");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel2.add(Pago3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 5);
        jPanel2.add(txtPago3, gridBagConstraints);

        Pago4.setText("Pago 4");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel2.add(Pago4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 5);
        jPanel2.add(txtPago4, gridBagConstraints);

        cmbPagos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 Pago Diferente", "2 Pagos Diferente", "3 Pagos Diferente", "4 Pagos Diferente" }));
        cmbPagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPagosActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        jPanel2.add(cmbPagos, gridBagConstraints);

        jLabel7.setText("TIempo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weighty = 0.1;
        jPanel2.add(jLabel7, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        jPanel2.add(txtTiempo, gridBagConstraints);

        getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

        setBounds(0, 0, 410, 308);
    }// </editor-fold>//GEN-END:initComponents

    private void cmbPagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPagosActionPerformed
        int index= cmbPagos.getSelectedIndex();
        switch(index){
            case 0:{
                Extras3(false);
                break;
            }
            case 1:{
                Extras1();
                break;
            }
            case 2:{
                Extras2();
                break;
            }
            case 3:{
                Extras3(true);
                break;
            }
        }
    }//GEN-LAST:event_cmbPagosActionPerformed
    public void Extras3(boolean val){
        txtTiempo.setEditable(false);
        if(val){
            Pago2.setVisible(val);
            Pago3.setVisible(val);
            Pago4.setVisible(val);
            txtPago2.setVisible(val);
            txtPago3.setVisible(val);
            txtPago4.setVisible(val);
           
        }else{
             Pago2.setVisible(val);
            Pago3.setVisible(val);
            Pago4.setVisible(val);
            txtPago2.setVisible(val);
            txtPago3.setVisible(val);
            txtPago4.setVisible(val);
           
        }
    }
    public void Extras1(){
            Pago2.setVisible(true);
            Pago3.setVisible(false);
            Pago4.setVisible(false);
            txtPago2.setVisible(true);
            txtPago3.setVisible(false);
            txtPago4.setVisible(false);
    }
    public void Extras2(){
            Pago2.setVisible(true);
            Pago3.setVisible(true);
            Pago4.setVisible(false);
            txtPago2.setVisible(true);
            txtPago3.setVisible(true);
            txtPago4.setVisible(false);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Pago1;
    private javax.swing.JLabel Pago2;
    private javax.swing.JLabel Pago3;
    private javax.swing.JLabel Pago4;
    private javax.swing.JComboBox<String> cmbPagos;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JFormattedTextField txtInversion;
    private javax.swing.JFormattedTextField txtPago1;
    private javax.swing.JFormattedTextField txtPago2;
    private javax.swing.JFormattedTextField txtPago3;
    private javax.swing.JFormattedTextField txtPago4;
    private javax.swing.JTextField txtTiempo;
    // End of variables declaration//GEN-END:variables
}
