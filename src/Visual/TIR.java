package Visual;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class TIR extends javax.swing.JInternalFrame {

    public TIR() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        Calcular = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        cmbPagos = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtInversion = new javax.swing.JFormattedTextField();
        txtTIR = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        Pago1 = new javax.swing.JLabel();
        txtPago1 = new javax.swing.JFormattedTextField();
        txtPago2 = new javax.swing.JFormattedTextField();
        txtPago3 = new javax.swing.JFormattedTextField();
        Pago2 = new javax.swing.JLabel();
        Pago3 = new javax.swing.JLabel();
        SpinPago1 = new javax.swing.JSpinner();
        SpinPago2 = new javax.swing.JSpinner();
        SpinPago3 = new javax.swing.JSpinner();
        Pago4 = new javax.swing.JLabel();
        txtPago4 = new javax.swing.JFormattedTextField();
        SpinPago4 = new javax.swing.JSpinner();

        setClosable(true);
        setMaximizable(true);
        setTitle("TIR");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icons/piggy-bank.png"))); // NOI18N

        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING));

        Calcular.setText("Calcular");
        Calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalcularActionPerformed(evt);
            }
        });
        jPanel1.add(Calcular);

        jButton2.setText("Cancelar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel3Layout = new java.awt.GridBagLayout();
        jPanel3Layout.columnWidths = new int[] {0, 5, 0, 5, 0};
        jPanel3Layout.rowHeights = new int[] {0, 5, 0, 5, 0, 5, 0};
        jPanel3.setLayout(jPanel3Layout);

        cmbPagos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 Pago Diferente", "2 Pagos Diferente", "3 Pagos Diferente", "4 Pagos Diferente" }));
        cmbPagos.setSelectedIndex(3);
        cmbPagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPagosActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel3.add(cmbPagos, gridBagConstraints);

        jLabel2.setText("Inversión");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel3.add(jLabel2, gridBagConstraints);

        txtInversion.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 3;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        jPanel3.add(txtInversion, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        jPanel3.add(txtTIR, gridBagConstraints);

        jLabel7.setText("TIR");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel3.add(jLabel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanel2.add(jPanel3, gridBagConstraints);

        java.awt.GridBagLayout jPanel4Layout = new java.awt.GridBagLayout();
        jPanel4Layout.columnWidths = new int[] {0, 5, 0, 5, 0, 5, 0, 5, 0};
        jPanel4Layout.rowHeights = new int[] {0, 17, 0, 17, 0, 17, 0, 17, 0};
        jPanel4.setLayout(jPanel4Layout);

        jLabel8.setText("# de veces");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(9, 0, 0, 11);
        jPanel4.add(jLabel8, gridBagConstraints);

        Pago1.setText("Pago 1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        jPanel4.add(Pago1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        jPanel4.add(txtPago1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        jPanel4.add(txtPago2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        jPanel4.add(txtPago3, gridBagConstraints);

        Pago2.setText("Pago 2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        jPanel4.add(Pago2, gridBagConstraints);

        Pago3.setText("Pago 3");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        jPanel4.add(Pago3, gridBagConstraints);

        SpinPago1.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 11);
        jPanel4.add(SpinPago1, gridBagConstraints);

        SpinPago2.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 11);
        jPanel4.add(SpinPago2, gridBagConstraints);

        SpinPago3.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 11);
        jPanel4.add(SpinPago3, gridBagConstraints);

        Pago4.setText("Pago 4");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weighty = 0.1;
        jPanel4.add(Pago4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        jPanel4.add(txtPago4, gridBagConstraints);

        SpinPago4.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 11);
        jPanel4.add(SpinPago4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanel2.add(jPanel4, gridBagConstraints);

        getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

        setBounds(0, 0, 537, 287);
    }// </editor-fold>//GEN-END:initComponents

    private void cmbPagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPagosActionPerformed
        int index= cmbPagos.getSelectedIndex();
        switch(index){
            case 0:{
                Extras3(false);
                break;
            }
            case 1:{
                Extras1();
                break;
            }
            case 2:{
                Extras2();
                break;
            }
            case 3:{
                Extras3(true);
                break;
            }
        }
    }//GEN-LAST:event_cmbPagosActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void CalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalcularActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CalcularActionPerformed
 public void Extras3(boolean val){
        txtTIR.setEditable(false);
        if(val){
            Pago2.setVisible(val);
            Pago3.setVisible(val);
            Pago4.setVisible(val);
            txtPago2.setVisible(val);
            txtPago3.setVisible(val);
            txtPago4.setVisible(val);
            SpinPago2.setVisible(val);
            SpinPago3.setVisible(val);
            SpinPago4.setVisible(val);
        }else{
             Pago2.setVisible(val);
            Pago3.setVisible(val);
            Pago4.setVisible(val);
            txtPago2.setVisible(val);
            txtPago3.setVisible(val);
            txtPago4.setVisible(val);
            SpinPago2.setVisible(val);
            SpinPago3.setVisible(val);
            SpinPago4.setVisible(val);
        }
    }
    public void Extras1(){
            Pago2.setVisible(true);
            Pago3.setVisible(false);
            Pago4.setVisible(false);
            txtPago2.setVisible(true);
            txtPago3.setVisible(false);
            txtPago4.setVisible(false);
            SpinPago2.setVisible(true);
            SpinPago3.setVisible(false);
            SpinPago4.setVisible(false);
    }
    public void Extras2(){
            Pago2.setVisible(true);
            Pago3.setVisible(true);
            Pago4.setVisible(false);
            txtPago2.setVisible(true);
            txtPago3.setVisible(true);
            txtPago4.setVisible(false);
            SpinPago2.setVisible(true);
            SpinPago3.setVisible(true);
            SpinPago4.setVisible(false);
    }
    public double Verificador(int i){
        double capturador=0;
        switch (i) {
            case 1:{
                try{
                    capturador= Double.parseDouble(txtPago1.getText());
                }catch(NumberFormatException ex){}
                break;
            }
            case 2:{
                try{
                    capturador= Double.parseDouble(txtPago2.getText());
                }catch(NumberFormatException ex){}
                break;
            }
            case 3:{
                try{
                    capturador= Double.parseDouble(txtPago3.getText());
                }catch(NumberFormatException ex){}
                break;
            }
            case 4:{
                try{
                    capturador= Double.parseDouble(txtPago4.getText());
                }catch(NumberFormatException ex){}
                break;
            }
        }
        return capturador;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Calcular;
    private javax.swing.JLabel Pago1;
    private javax.swing.JLabel Pago2;
    private javax.swing.JLabel Pago3;
    private javax.swing.JLabel Pago4;
    private javax.swing.JSpinner SpinPago1;
    private javax.swing.JSpinner SpinPago2;
    private javax.swing.JSpinner SpinPago3;
    private javax.swing.JSpinner SpinPago4;
    private javax.swing.JComboBox<String> cmbPagos;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JFormattedTextField txtInversion;
    private javax.swing.JFormattedTextField txtPago1;
    private javax.swing.JFormattedTextField txtPago2;
    private javax.swing.JFormattedTextField txtPago3;
    private javax.swing.JFormattedTextField txtPago4;
    private javax.swing.JTextField txtTIR;
    // End of variables declaration//GEN-END:variables
}
