package Visual;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class IR extends javax.swing.JInternalFrame {

    public IR() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        btnCalcular = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        Pago1 = new javax.swing.JLabel();
        txtPago1 = new javax.swing.JFormattedTextField();
        Pago2 = new javax.swing.JLabel();
        txtPago2 = new javax.swing.JFormattedTextField();
        Pago3 = new javax.swing.JLabel();
        txtPago3 = new javax.swing.JFormattedTextField();
        Pago4 = new javax.swing.JLabel();
        txtPago4 = new javax.swing.JFormattedTextField();
        jPanel4 = new javax.swing.JPanel();
        cmbPagos = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtInteres = new javax.swing.JFormattedTextField();
        txtInversion = new javax.swing.JFormattedTextField();
        txtIRent = new javax.swing.JTextField();

        setClosable(true);
        setMaximizable(true);
        setTitle("Calculo de a IRenta");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icons/bar-chart.png"))); // NOI18N

        btnCalcular.setText("Calcular");
        jPanel1.add(btnCalcular);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancelar);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        java.awt.GridBagLayout jPanel3Layout = new java.awt.GridBagLayout();
        jPanel3Layout.columnWidths = new int[] {0, 16, 0};
        jPanel3Layout.rowHeights = new int[] {0, 17, 0, 17, 0, 17, 0};
        jPanel3.setLayout(jPanel3Layout);

        Pago1.setText("Pago 1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel3.add(Pago1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.2;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 5);
        jPanel3.add(txtPago1, gridBagConstraints);

        Pago2.setText("Pago 2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel3.add(Pago2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 5);
        jPanel3.add(txtPago2, gridBagConstraints);

        Pago3.setText("Pago 3");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel3.add(Pago3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 5);
        jPanel3.add(txtPago3, gridBagConstraints);

        Pago4.setText("Pago 4");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel3.add(Pago4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 5);
        jPanel3.add(txtPago4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanel2.add(jPanel3, gridBagConstraints);

        java.awt.GridBagLayout jPanel4Layout = new java.awt.GridBagLayout();
        jPanel4Layout.columnWidths = new int[] {0, 16, 0, 16, 0};
        jPanel4Layout.rowHeights = new int[] {0, 17, 0, 17, 0, 17, 0, 17, 0};
        jPanel4.setLayout(jPanel4Layout);

        cmbPagos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 Pago Diferente", "2 Pagos Diferente", "3 Pagos Diferente", "4 Pagos Diferente" }));
        cmbPagos.setSelectedIndex(3);
        cmbPagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPagosActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 0);
        jPanel4.add(cmbPagos, gridBagConstraints);

        jLabel1.setText("Tasa de Interes");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 0);
        jPanel4.add(jLabel1, gridBagConstraints);

        jLabel2.setText("Inversión");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 0);
        jPanel4.add(jLabel2, gridBagConstraints);

        jLabel7.setText("IRent");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 0);
        jPanel4.add(jLabel7, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.2;
        jPanel4.add(txtInteres, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        jPanel4.add(txtInversion, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.ipady = 2;
        gridBagConstraints.weightx = 0.1;
        jPanel4.add(txtIRent, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        jPanel2.add(jPanel4, gridBagConstraints);

        getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

        setBounds(0, 0, 474, 308);
    }// </editor-fold>//GEN-END:initComponents

    private void cmbPagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPagosActionPerformed
        int index= cmbPagos.getSelectedIndex();
        switch(index){
            case 0:{
                Extras3(false);
                break;
            }
            case 1:{
                Extras1();
                break;
            }
            case 2:{
                Extras2();
                break;
            }
            case 3:{
                Extras3(true);
                break;
            }
        }
    }//GEN-LAST:event_cmbPagosActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed
 public void Extras3(boolean val){
        txtIRent.setEditable(false);
        if(val){
            Pago2.setVisible(val);
            Pago3.setVisible(val);
            Pago4.setVisible(val);
            txtPago2.setVisible(val);
            txtPago3.setVisible(val);
            txtPago4.setVisible(val);
        }else{
             Pago2.setVisible(val);
            Pago3.setVisible(val);
            Pago4.setVisible(val);
            txtPago2.setVisible(val);
            txtPago3.setVisible(val);
            txtPago4.setVisible(val);
        }
    }
    public void Extras1(){
            Pago2.setVisible(true);
            Pago3.setVisible(false);
            Pago4.setVisible(false);
            txtPago2.setVisible(true);
            txtPago3.setVisible(false);
            txtPago4.setVisible(false);
       
    }
    public void Extras2(){
            Pago2.setVisible(true);
            Pago3.setVisible(true);
            Pago4.setVisible(false);
            txtPago2.setVisible(true);
            txtPago3.setVisible(true);
            txtPago4.setVisible(false);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Pago1;
    private javax.swing.JLabel Pago2;
    private javax.swing.JLabel Pago3;
    private javax.swing.JLabel Pago4;
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox<String> cmbPagos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField txtIRent;
    private javax.swing.JFormattedTextField txtInteres;
    private javax.swing.JFormattedTextField txtInversion;
    private javax.swing.JFormattedTextField txtPago1;
    private javax.swing.JFormattedTextField txtPago2;
    private javax.swing.JFormattedTextField txtPago3;
    private javax.swing.JFormattedTextField txtPago4;
    // End of variables declaration//GEN-END:variables
}
