package Visual;

import Controladores.DepreciacionControlador;


/**
 *
 * @author Flavio Cesar Arauz
 */
public class FrmDepreciacion extends javax.swing.JInternalFrame {
     private DepreciacionControlador DepCrtl;
 
    public FrmDepreciacion() {
        DepCrtl= new DepreciacionControlador();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Type = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        btnGenerar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TblDep = new javax.swing.JTable();

        setClosable(true);
        setForeground(java.awt.Color.lightGray);
        setMaximizable(true);
        setTitle("Depreciacion");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icons/dollar-symbol-1.png"))); // NOI18N

        java.awt.FlowLayout flowLayout1 = new java.awt.FlowLayout(java.awt.FlowLayout.TRAILING);
        flowLayout1.setAlignOnBaseline(true);
        jPanel2.setLayout(flowLayout1);

        btnGenerar.setText("Generar");
        btnGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarActionPerformed(evt);
            }
        });
        jPanel2.add(btnGenerar);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel2.add(btnCancelar);

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_END);

        jPanel3.setLayout(new java.awt.BorderLayout());

        TblDep.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(TblDep);

        jPanel3.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        setBounds(0, 0, 614, 412);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarActionPerformed
        DlgDepreciacion dd = new DlgDepreciacion( null, true);
        dd.setVisible(true);
        TblDep.setModel(DepCrtl.ObtenerTabla());
        setPreferredWidth();
    }//GEN-LAST:event_btnGenerarActionPerformed
  
    private void setPreferredWidth() {
        TblDep.setColumnModel(
                DepCrtl.GenerarColumna(TblDep.getColumnModel(),
                        new int[]{1, 3, 3, 3}, new int[]{150, 100, 120, 120}));
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TblDep;
    private javax.swing.ButtonGroup Type;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGenerar;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
