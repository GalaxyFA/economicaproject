/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gestiones;
import static java.lang.Math.pow;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class AnuaInt {
    private double inte;
    
    public AnuaInt() {
    }
    
     public double PresenteGradienteArimetica(double grad,double interes, int periodo){
        double gradiente=0;
        inte=pow((1+interes), -periodo);
        gradiente= (grad/interes)*(((1-inte)/interes)-(periodo/(pow((1+interes), periodo))));
        return gradiente;
    }
    public double FuturoGradienteArimetica(double grad, double interes, int periodo){
         double gradiente=0;
        inte= pow((1+interes),periodo)/interes; 
        gradiente= (grad/interes)*(inte-periodo);
        return gradiente;
    }
    public double PresenteAnuaNormal(double anua, double interes , int periodo){
        double resultado =0;
        inte=pow((1+interes), -periodo);
       resultado=  anua*((1-inte)/interes);
       return resultado;
    }
    public double FuturoAnualNormal(double anua, double interes , int periodo){
          double resultado =0;
        inte=pow((1+interes), periodo);
       resultado=  anua*((inte-1)/interes);
       return resultado;
    }
    public double PresenteAnuaAnticipada(double anua, double interes , int periodo){
      double resultado =0;
      inte=pow(1+interes, -periodo);
      resultado= anua*(1+interes)*((1-inte)/interes);
      return resultado;
    }
    public double FuturoAnuaAnticipada(double anua, double interes , int periodo){
       double resultado= 0;
        inte=pow(1+interes, periodo);
        resultado= anua*((inte-1)/interes)*(1+interes);
        return resultado;
    }
    public double AnuaDiferida(double anua, double interes , int periodo, int m){
        double resultado=0;
        double inte2= Math.pow(1+interes, -m);
        inte=pow(1+interes, -periodo);
        resultado= anua*((1-inte)/interes)*inte2;
        return resultado;
    }
    public double PresenteGradienteGeometrico(double anua, double interes, double grad, int periodo){
        double resultado= 0;
        double grand= pow(1+grad, periodo);
        inte=pow(1+interes, periodo);
        resultado= (anua/(grad- interes))*((grand/inte)-1);
        return resultado;
    }
    public double FuturoGradienteGeometricodouble(double anua, double interes, double grad, int periodo){
        double resultado= 0;
        double grand= pow(1+grad, periodo);
        inte=pow(1+interes, periodo);
        resultado= (anua/(grad- interes))*(grand-inte);
        return resultado;
    }
}
