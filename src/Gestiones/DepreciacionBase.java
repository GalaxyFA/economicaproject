
package Gestiones;

/**
 *
 * @author Flavio Cesar Arauz
 */
import Raiz.Depreciacion;
import java.util.*;
public class DepreciacionBase {
    private List<Depreciacion> depreciaciones;

    public DepreciacionBase() {
      depreciaciones= DatosDepreciados.DarDatos();
    }
    public void Guardar(Depreciacion depreciacion) {
        depreciaciones.add(depreciacion);
    }
    public List<Depreciacion> Mostrar(){
        return depreciaciones;
    }
    public void refresh(){
        depreciaciones.clear();
    }
}
