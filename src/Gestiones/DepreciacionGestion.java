/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gestiones;

import Raiz.Depreciacion;
import java.util.List;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class DepreciacionGestion {
  private DepreciacionBase depreciacionBase;

    public DepreciacionGestion() {
        depreciacionBase= new DepreciacionBase();
    }

    public DepreciacionGestion(DepreciacionBase depreciacionBase) {
        this.depreciacionBase = depreciacionBase;
    }
    public void GuardarDepreciacion(Depreciacion depreciacion){
      depreciacionBase.Guardar(depreciacion);
    }
    public List<Depreciacion> MostrarDepreciaciones(){
        return  depreciacionBase.Mostrar();
    }
    public void Actualizar(){
        depreciacionBase.refresh();
    }
}
