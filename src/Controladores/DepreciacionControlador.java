
package Controladores;


import Gestiones.DepreciacionGestion;
import Raiz.Depreciacion;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
/**
 *
 * @author Flavio Cesar Arauz
 */
public class DepreciacionControlador {
   private DepreciacionGestion dgestion;
   private String Cabezilla[]={"Año/Mes","Depreciacion","Depreciacion Acumulada","Valor Restante"};

    public DepreciacionControlador() {
        dgestion = new DepreciacionGestion();
    }

    public DepreciacionControlador(DepreciacionGestion dgestion) {
        this.dgestion = dgestion;
    }

    public DepreciacionGestion getDgestion() {
        return dgestion;
    }

    public String[] getCabezilla() {
        return Cabezilla;
    }
    public Object[][] ObtenerDatos(){
        return ObtenerDatos(dgestion.MostrarDepreciaciones());
    }

    private Object[][] ObtenerDatos(List<Depreciacion> lista) {
        Object [][] datos = null;
        
        if(lista.isEmpty()){
            return datos;
        }
        datos = new Object[lista.size()][Cabezilla.length];
        int i = 0;
        
        for(Depreciacion dep: lista){
            datos[i]= DarDepreciacionDeArreglo(dep);
            i++;
        }
        return datos;
    }

    private Object[] DarDepreciacionDeArreglo(Depreciacion dep) {
        Object[] fila= new Object[4];
        fila[0]= dep.getAnio();
        fila[1]= dep.getDepreciacion();
        fila[2]= dep.getDepreciacion_acumulada();
        fila[3]= dep.getValoranual();
        return fila;
    }
    public TableModel ObtenerTabla(){
        return new DefaultTableModel(ObtenerDatos(), Cabezilla);
    }
    public TableColumnModel GenerarColumna(TableColumnModel tcm, 
        int indice[], int w[]){
        int a=0;
        for(int i:indice){
            tcm.getColumn(i).setPreferredWidth(w[a++]);
        }
        return tcm;
    }
    
}
