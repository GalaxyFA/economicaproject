/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Gestiones.DepreciacionGestion;
import Raiz.Depreciacion;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class ControladorPrincipal {
    private DepreciacionGestion dgestion;

    public ControladorPrincipal() {
        dgestion= new DepreciacionGestion();
    }
    public DepreciacionGestion darDgestion(){
        return dgestion;
    }
}
