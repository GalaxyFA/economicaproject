/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Raiz;

/**
 *
 * @author Flavio Cesar Arauz
 */
public class Depreciacion {
    private int anio;
    private double depreciacion;
    private double depreciacion_acumulada;
    private double valoranual;

   public Depreciacion(int anio, double depreciacion, double depreciacion_acumulada, double valoranual) {
        this.anio = anio;
        this.depreciacion = depreciacion;
        this.depreciacion_acumulada = depreciacion_acumulada;
        this.valoranual = valoranual;
    }
    
    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public double getDepreciacion() {
        return depreciacion;
    }

    public void setDepreciacion(double depreciacion) {
        this.depreciacion = depreciacion;
    }

    public double getValoranual() {
        return valoranual;
    }

    public void setValoranual(double valoranual) {
        this.valoranual = valoranual;
    }
    public double getDepreciacion_acumulada() {
        return depreciacion_acumulada;
    }

    public void setDepreciacion_acumulada(double depreciacion_acumulada) {
        this.depreciacion_acumulada = depreciacion_acumulada;
    }
}
